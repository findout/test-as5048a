#include <Arduino.h>
#include <AS5048A.h>
#include <Stepper.h>

const int stepsPerRevolution = 64*64;  // change this to fit the number of steps per revolution

Stepper myStepper(stepsPerRevolution, D1, D3, D2, D4);
AS5048A angleSensor(D8);

void setup()
{
  Serial.begin(9600);
	myStepper.setSpeed(5);
	angleSensor.init();
}

void readAngle() {
	word val = angleSensor.getRawRotation();
	Serial.print("Got rotation of: 0x");
	Serial.println(val, HEX);
	Serial.print("State: ");
	angleSensor.printState();
	Serial.print("Errors: ");
	Serial.println(angleSensor.getErrors());
}
int readLine(char *buf, int bufLen) {
  static int pos = 0;

  int readch = Serial.read();
  if (readch > 0) {
    switch (readch) {
    case '\n': // Ignore new-lines
      break;
    case '\r': // Return on CR
      pos = 0;  // Reset position index ready for next time
      Serial.println();
      return 1;
    default:
      Serial.write(readch);
      if (pos < bufLen-1) {
        buf[pos++] = readch;
        buf[pos] = 0;
      }
    }
  }
  return 0;
}

char buf[50];

uint8 motorPins[] = {D1, D2, D3, D4};
uint8 motorSignals[] = {
	0b1000,
	0b1100,
	0b0100,
	0b0110,
	0b0010,
	0b0011,
	0b0001,
	0b1001
};

int stepsLeft = 0;
uint8 phase = 0;

void moveSteps(int n) {
	stepsLeft += n;
}

void run() {
	if (stepsLeft != 0) {
		int8 move = stepsLeft < 0 ? 1 : -1;
		uint8 signals = motorSignals[phase];
		digitalWrite(motorPins[0], (signals & 1) != 0);
		digitalWrite(motorPins[1], (signals & 2) != 0);
		digitalWrite(motorPins[2], (signals & 4) != 0);
		digitalWrite(motorPins[3], (signals & 8) != 0);
		delay(2);
		phase = (phase + move) & 7;
		stepsLeft += move;
	} else {
		digitalWrite(motorPins[0], 0);
		digitalWrite(motorPins[1], 0);
		digitalWrite(motorPins[2], 0);
		digitalWrite(motorPins[3], 0);
	}
}

void loop() {
	if (readLine(buf, sizeof buf)) {
		if (buf[0] == 'r') {
			word val = angleSensor.getRawRotation();
			Serial.println(val * 360 / 0x4000);
			// Serial.printf("%3.0f", )
		} else {
			moveSteps(atoi(buf));
		}
	}
	// if (stepsLeft == 0) {
	// 	moveSteps(4096 / 8);
	// 	delay(500);
	// }
	run();
}